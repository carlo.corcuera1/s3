import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
    <App />
  </>
);

// const name = "John Smith";
// const element = <h1>Hello, {name}</h1>

// root.render(element);

// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// };

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>

// root.render(element);

// JSX allows us to create HTML elements and at the same time allows us to apply JavaScript code to these elements making it easy to write both HTML and Javascript code in a single file.


