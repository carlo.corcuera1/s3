import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process



function App() {

  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  // const [user, setUser] = useState({
  //   email: localStorage.getItem("email")
  // })
  // Use console.log(user), to check the value of our state hook
  //console.log(user);
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      console.log(data);

      // User is logged in
      if(typeof data._id !== 'undefined'){
          setUser({
            id: data._id ,
            isAdmin: data.isAdmin
          })
      // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, []);

  return (
    // Common pattern in react.js for a component to return multiple elem
    <>
      {/*Initializes that dynamic routing will be involved*/}
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
          <Container>
            <Routes >
              <Route path="/" element={<Home />}/>
              <Route path="/courses" element={<Courses />}/>
              <Route path="/courses/:courseId" element={<CourseView />}/>
              <Route path="/login" element={<Login />}/>
              <Route path="/register" element={<Register />}/>
              <Route path="/logout" element={<Logout />}/>
              <Route path="*" element={<Error />}/>
            </Routes>
          </Container>
      </Router>
      </UserProvider>
    </>
  );
}

export default App;

