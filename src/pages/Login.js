import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'

import Swal from 'sweetalert2';

export default function Login() {

    // Allows us to consume the User context object and it's properties to use for user validation 
    const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    //const navigate = useNavigate();

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // "fetch" method is used to send request in the server and load the received response in the webpage
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            // Sets the header data "request" object to be sent to the backend
            headers: {
                'Content-Type': 'application/json'
            },
            // JSON.stringify converts the object data into stringified JSON
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        // The first ".then" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
        .then(res => res.json())
        .then(data => {

            // It is good practice to always print out the result of our fetch request to ensure that the correct information is received in our frontend application
            // We will recieve either a token or an error response
            console.log(data);

            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "Please, check your login details and try again!"
                })
            }

        })

        // Set the email of the authenticated user in the local storage 
            // Syntax
            // localStorage.setItem('propertyName', value)

        // In this case, the key name is 'email' and the value is the value of the email variable. This code sets the value of the 'email' key in the local storage to the value of the email variable. Once the value is stored in the local storage, you can retrieve it using the getItem method of the localStorage object:

        // localStorage.setItem('email', email);
        // console.log(email);

        // Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
        // When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering
        // setUser({
        //     email: localStorage.getItem('email')
        // });

        // Clear input fields after submission
        setEmail('');
        setPassword('');
        //navigate('/');

        //alert(`"${email}" has been verified! Welcome back!`);

    };

    const retrieveUserDetails = (token) => {
                    
        // The token will be sent as part of the request's header information
        // We put "Bearer" in front of the token to follow implementation standards for JWTs
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            // Global user state for validation across the whole app
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });

        })

    };


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);


    return (
        (user.id !== null) ? 
            <Navigate to="/courses" />

        :

        <Form onSubmit={(e) => authenticate(e)}>

            <Form.Group controlId="userEmail" className="my-2">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
        			onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password" className="my-2">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
        			onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary my-3" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger my-3" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }

        </Form>
    )
}

